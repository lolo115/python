# -*- coding: utf-8 -*-

# [spark@spark python]$ spark-submit ./csv.py
# root
#  |-- Code région: string (nullable = true)
#  |-- Nom de la région: string (nullable = true)
#  |-- Code département: string (nullable = true)
#  |-- Code arrondissement: string (nullable = true)
#  |-- Code canton: string (nullable = true)
#  |-- Code commune: string (nullable = true)
#  |-- Nom de la commune: string (nullable = true)
#  |-- Population municipale: string (nullable = true)
#  |-- Population comptée à part: string (nullable = true)
#  |-- Population totale: string (nullable = true)
#
# +-----------+--------------------+----------------+-------------------+-----------+------------+--------------------+---------------------+-------------------------+-----------------+
# |Code région|    Nom de la région|Code département|Code arrondissement|Code canton|Code commune|   Nom de la commune|Population municipale|Population comptée à part|Population totale|
# +-----------+--------------------+----------------+-------------------+-----------+------------+--------------------+---------------------+-------------------------+-----------------+
# |         84|Auvergne-Rhône-Alpes|              01|                  2|         08|         001|L' Abergement-Clé...|                  767|                       18|              785|
# |         84|Auvergne-Rhône-Alpes|              01|                  1|         01|         002|L' Abergement-de-...|                  239|                        2|              241|
# |         84|Auvergne-Rhône-Alpes|              01|                  1|         01|         004|   Ambérieu-en-Bugey|                14022|                      535|            14557|
# |         84|Auvergne-Rhône-Alpes|              01|                  2|         22|         005| Ambérieux-en-Dombes|                 1627|                       31|             1658|
# |         84|Auvergne-Rhône-Alpes|              01|                  1|         04|         006|             Ambléon|                  109|                        6|              115|
# +-----------+--------------------+----------------+-------------------+-----------+------------+--------------------+---------------------+-------------------------+-----------------+
# only showing top 5 rows
#
# root
#  |-- NomCommune: string (nullable = true)
#  |-- PopTot: float (nullable = true)
#
# +--------------------+-------+
# |          NomCommune| PopTot|
# +--------------------+-------+
# |L' Abergement-Clé...|  785.0|
# |L' Abergement-de-...|  241.0|
# |   Ambérieu-en-Bugey|14557.0|
# | Ambérieux-en-Dombes| 1658.0|
# |             Ambléon|  115.0|
# +--------------------+-------+
# only showing top 5 rows


from __future__ import print_function

import sys
from random import random
from operator import add

from pyspark.sql import SparkSession
from pyspark.sql.functions import *


if __name__ == "__main__":
    spark = SparkSession\
        .builder\
        .appName("Python CSV")\
        .getOrCreate()

    df = spark.read.csv(path="sample/fr_town_population", header=True, sep=";", encoding="utf-8")
    df.printSchema()
    df.show(5)
    df.unpersist()

    # permet le subsetting du dataframe DF en renommant les colonnes (withColumn), en castant une des deux colonnes (withColum + cast), et en subsettant au niveau colonne (select)
    df2=df.withColumn("NomCommune",df["Nom de la commune"]).withColumn("PopTot",df["Population totale"].cast("float")).select("NomCommune","PopTot")
    df2.printSchema()
    df2.show(5)
    spark.stop()
